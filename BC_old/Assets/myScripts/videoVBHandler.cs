﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class videoVBHandler : MonoBehaviour , IVirtualButtonEventHandler {

    public GameObject videoGO;
    void Start()
    {
        VirtualButtonBehaviour[] vrb = GetComponentsInChildren<VirtualButtonBehaviour>();
        for (int i = 0; i < vrb.Length; i++) {
            vrb[i].RegisterEventHandler(this);
        }
        videoGO.SetActive(false);


    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnButtonPressed(VirtualButtonAbstractBehaviour vb)
    {
        if (vb.VirtualButtonName == "videoVB")
        {
            videoGO.SetActive(true);
        }
        else
        {
            throw new System.NotImplementedException();
        }
    }

    public void OnButtonReleased(VirtualButtonAbstractBehaviour vb)
    {
        Debug.Log("virtual button relased");
    }

    // Use this for initialization
   
}
